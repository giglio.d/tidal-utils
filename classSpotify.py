import spotipy

class ClSpotify:
    def __init__(self):
        credentials_manager = spotipy.SpotifyOAuth(username=config['username'],
				                   scope='playlist-read-private',
				                   client_id=config['client_id'],
				                   client_secret=config['client_secret'],
				                   redirect_uri=config['redirect_uri'])
        try:
            credentials_manager.get_access_token(as_dict=False)
        except spotipy.SpotifyOauthError:
            sys.exit("Error opening Spotify sesion; could not get token for username: ".format(config['username']))

        spotipy.Spotify(oauth_manager=credentials_manager)
