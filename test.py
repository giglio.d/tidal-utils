#!/usr/bin/env python3

import argparse
import datetime
import json
import requests
import sys
import webbrowser
import yaml

from classSpotify import ClSpotify
from classTidal import ClTidal


def serialize_instance(obj):
    d = { '__classname__' : type(obj).__name__ }
    d.update(vars(obj))
    return d

def get_tidal_favorite_tracks_OLD():
    page = 0
    ltracks = tidalapi.user.Favorites(tidal_session,187003374).tracks(500, page)
    # for track in ltracks:
    #     print(track.artist.name + ' : ' + track.name)
    # print(str(page) + ': ' + str(len(ltracks)))
    while len(ltracks) == 500:
        page += 500
        ltracks = tidalapi.user.Favorites(tidal_session,187003374).tracks(500, page)
    #    print(str(page) + ': ' + str(len(ltracks)))

        for track in ltracks:
            if not track.available:
                print(track.artist.name + ' ' + track.name + ' : ' + track.album.name + ' : ' + str(track.album.year))

    page += 500
    ltracks = tidalapi.user.Favorites(tidal_session,187003374).tracks(500, page)
    for track in ltracks:
        if not track.available:
            print(track.artist.name + ' ' + track.name + ' : ' + track.album.name + ' : ' + str(track.album.year))
    # global totalNumberOfItems
    # totalNumberOfItems = response.json()[1]
    # return sorted(response.json()['items'], key=lambda item: datetime.datetime.strptime(item["created"], '%Y-%m-%dT%H:%M:%S.%f%z'))


def print_type_tracks():
    page = 0
    ltracks = tidalapi.user.Favorites(tidal_session,187003374).tracks(500, page)
    print(type(ltracks[0]))



# def get_genre():

if __name__ == '__main__':
    global tidal_session

    parser = argparse.ArgumentParser()
    parser.add_argument('--config', default='config.yml', help='location of the config file')
    parser.add_argument('--uri', help='synchronize a specific URI instead of the one in the config')
    args = parser.parse_args()

    with open(args.config, 'r') as f:
        config = yaml.safe_load(f)

    tidal = ClTidal()
    # spotify_session = open_spotify_session(config['spotify'])

    # lstTracks = tidal.getFavouriteTracks()
    # [ print(t.artist.id) for t in lstTracks ]
    # print(str(len(lstTracks)))

    # lstArtists = tidal.getFavouriteArtists()
    # [ print(a.name) for a in lstArtists ]
    # print(str(len(lstArtists)))

    # print(lstTracks[0].name)
    # a = tidal.getTrackArtist(lstTracks[0])
    # tidal.addArtist(a.id)

    tidal.followArtist()
    # tidal.loadFavArtistsIds()

# spotify = spotipy.Spotify(auth_manager=SpotifyClientCredentials())

# for artist in lstArtists:
#     results = spotify.search(q='artist:' + artist.name, type='artist')

# print(type(results[0]))
