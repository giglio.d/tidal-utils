import tidalapi
import yaml
import pdb

class ClTidal:
    def __init__(self):
        session = tidalapi.Session()
        page = 1000
        offset = 0

        try:
            with open('.session.yml', 'rb') as session_file:
                previous_session = yaml.safe_load(session_file)
                logged_in = session.load_oauth_session(previous_session['token_type'],
                                                       previous_session['access_token'],
                                                       previous_session['refresh_token'])

                assert logged_in == True
        except OSError:
            session.login_oauth_simple()
            token_type = session.token_type
            access_token = session.access_token
            refresh_token = session.refresh_token # Not needed if you don't care about refreshing
            expiry_time = session.expiry_time
            with open(SESSION_DATA_FILENAME, 'wb') as out:
                pickle.dump({"token_type": token_type,
                             "access_token": access_token,
                             "refresh_token": refresh_token}, session_file)

        self.session = session

    def getFavouriteArtists(self):
        lartists = []
        subset = []
        page = 1000
        offset = 0

        subset = tidalapi.user.Favorites(self.session,187003374).artists(page, offset)
        print(f'subset size is {len(subset)}')
        lartists = subset

        while len(subset) == page:
            print(f'subset size is {len(subset)}')
            offset += page
            subset = tidalapi.user.Favorites(self.session,187003374).artists(page, offset)
            lartists.extend(subset)

        # offset += page
        # lartists.extend(tidalapi.user.Favorites(self.session,187003374).artists(page, offset))

        return lartists

    def getFavouriteAlbums(self):
        lalbums = []
        subset = []
        page = 1000
        offset = 0

        subset = tidalapi.user.Favorites(self.session,187003374).albums(page, offset)
        lalbums = subset

        while len(subset) == page:
            offset += page
            subset = tidalapi.user.Favorites(self.session,187003374).albums(page, offset)
            lalbums.extend(subset)

        # offset += page
        # lalbums = tidalapi.user.Favorites(self.session,187003374).albums(page, offset)

        return lalbums

    def getFavouriteTracks(self):
        ltracks = []
        subset = []
        page = 1000
        offset = 0

        subset = tidalapi.user.Favorites(self.session,187003374).tracks(page, offset)
        ltracks = subset

        while len(subset) == page:
            offset += page
            subset = tidalapi.user.Favorites(self.session,187003374).tracks(page, offset)
            ltracks.extend(subset)

        # ltracks.extend(subset)
        # offset += page
        # ltracks.extend(tidalapi.user.Favorites(self.session,187003374).tracks(page, offset))

        return ltracks

    def getTrackArtist(self, track):
        return track.artist

    def addArtist(self, id):
        return tidalapi.user.Favorites(self.session,187003374).add_artist(id)

    def followArtist(self):
        lstTracks = self.getFavouriteTracks()
        print(f'n of tracks: {len(lstTracks)}')
        lstArtistsObj = self.getFavouriteArtists()
        print(f'n of artists: {len(lstArtistsObj)}')
        tArtists = ([ a.id for a in lstArtistsObj ])
        print(f'size of tArtist is {len(tArtists)}')

        fh = open("artisti.txt", "w")
        print([ a.id for a in lstArtistsObj ],file=fh)
        fh.close()
        # print(tArtists)
        orphan_tracks = []
        for t in lstTracks :
            if t.artist.id not in tArtists :
                # print(f'{t.artist.id:10} :: {t.artist.name}')
                orphan_tracks.extend(str(t.artist.id))

        # fh = open("tracce.txt", "w")
        # print(str(orphan_tracks),file=fh)
        # fh.close()

    def saveFavArtistsIds(self):
        lstArtists = self.getFavouriteArtists()
        fh = open("artisti.txt", "w")
        print([ a.id for a in lstArtists ],file=fh)
        fh.close()

    def loadFavArtistsIds(self):
        fh = open("artisti.txt", "r")
        data = fh.read()
        fh.close()

        print(type(data))
        print(type(data[0]))
